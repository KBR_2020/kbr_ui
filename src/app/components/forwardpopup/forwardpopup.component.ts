import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ChatService} from '../../services/chat.service';

export interface DialogData {
  fImage: any;
  msg: string;
}
@Component({
  selector: 'app-forwardpopup',
  templateUrl: './forwardpopup.component.html',
  styleUrls: ['./forwardpopup.component.scss']
})
export class ForwardpopupComponent implements OnInit {

  chatData: any = [];
  constructor(public dialogRef: MatDialogRef<ForwardpopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData , private service: ChatService) { }

  ngOnInit(): void {
    this.service.getData().then(data => {
      this.chatData = data;
    });
  }
  closeDialog() {
    this.dialogRef.close(true);
  }

}
