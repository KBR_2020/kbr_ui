import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ChatListPopOverComponent} from './chat-list-pop-over/chat-list-pop-over.component';
import {MatMenuModule} from '@angular/material/menu';
import {FormsModule} from '@angular/forms';
import { ForwardpopupComponent } from './forwardpopup/forwardpopup.component';



@NgModule({
  declarations: [ChatListPopOverComponent, ForwardpopupComponent],
    imports: [
        CommonModule,
        MatMenuModule,
        FormsModule
    ]
})
export class ComponentsModule { }
