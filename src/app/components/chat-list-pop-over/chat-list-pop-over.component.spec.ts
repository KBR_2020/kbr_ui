import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatListPopOverComponent } from './chat-list-pop-over.component';

describe('ChatListPopOverComponent', () => {
  let component: ChatListPopOverComponent;
  let fixture: ComponentFixture<ChatListPopOverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatListPopOverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatListPopOverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
