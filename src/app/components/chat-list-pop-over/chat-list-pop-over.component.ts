import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {Message} from '../../model/Message';
import {environment} from '../../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {ChatService} from '../../services/chat.service';
import {DataService} from '../../services/data.service';
import {SocketService} from '../../services/socket.service';
import {UploadfileService} from '../../services/uploadfile.service';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';

@Component({
  selector: 'app-chat-list-pop-over',
  templateUrl: './chat-list-pop-over.component.html',
  styleUrls: ['./chat-list-pop-over.component.scss']
})
export class ChatListPopOverComponent implements OnInit {
  data: any = {};
  userMessages: any = [];
  message = '';
  stompClient = null;
  isLoaded = false;
  isCustomSocketOpened = false;
  messages: Message[] = [];
  selectedFiles: FileList;
  private serverUrl = environment.url + 'ws';
  file = [];
  constructor(private bottomSheetRef: MatBottomSheetRef<ChatListPopOverComponent>,
              private route: ActivatedRoute, private router: Router,
              private service: ChatService, private dataService: DataService,
              private uploadService: UploadfileService,
              private  socketService: SocketService, @Inject(MAT_BOTTOM_SHEET_DATA) public profileData: any) {
    this.data = this.profileData;
    console.log('hi' + this.data);
  }


  ngOnInit(): void {
    this.file = [
       {filename: 'adsc.jpg', filelocation: '../../../assets/img/course-img.jpg'},
       {filename: 'rain.mp4', filelocation: '../../../assets/img/rain.mp4 '},
      {filename: 'user.rar', filelocation: '../../../assets/img/user.rar'},
      {filename: 'user1.zip', filelocation: '../../../assets/img/user1.zip'},
     // {filename: 'demo.html', filelocation: 'https://fontawesome.com/v4.7.0/icons/'},
      {filename: 'adsc.jpg', filelocation: '../../../assets/img/course-img.jpg'},
      {filename: 'adsc.jpg', filelocation: '../../../assets/img/course-img.jpg'},

    ];
    const ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    this.stompClient.connect({}, () => {
      that.isLoaded = true;
      that.openSocket();
    });
    console.log(this.data.id);
  }
  openSocket(): void {
    if (this.isLoaded) {
      this.isCustomSocketOpened = true;
      this.stompClient.subscribe('/socket-publisher/' + this.data.id, (message) => {
        this.handleResult(message);
      });
    }
  }
  /*sendMessage(): void {
    if (this.data.id) {
      const message: Message = { message: this.message, fromId: this.data.id, toId: this.data.name, fileName: '',
        fileSize: 0 , fileType: ''};
      this.stompClient.send('/socket-publisher/send/message', {}, JSON.stringify(message));
    }
  }*/

  sendMessageUsingRest(): void {
    /* console.log(1);
     const formData: FormData = new FormData();
     if (this.message !== '') {
       console.log(2);
       if (this.selectedFiles === undefined) {
         console.log(3);
         if (this.data.id) {
           console.log(4);
           const message: Message = {
             message: this.message, fromId: this.data.id, toId: this.data.name, fileName: '',
             fileSize: 0, fileType: ''
           };
           this.socketService.post(message).subscribe(res => {
             this.selectedFiles = undefined;
             console.log(res);
           });
         }
       } else {
         console.log(5);
         const message: Message = {
           message: this.message, fromId: this.data.id, toId: this.data.name, fileName: '',
           fileSize: 0, fileType: ''
         };
         formData.append('message', JSON.stringify({message: this.message, fromId: this.data.id, toId: this.data.name}));
         formData.append('file', this.selectedFiles.item(0));
         this.uploadService.pushFileToStorage(formData).subscribe(data => {
           this.selectedFiles = undefined;
           console.log(data);
         });
       }
     }else if (this.message === '' && this.selectedFiles !== undefined){
       console.log(6);
       const message: Message = {
         message: this.message, fromId: this.data.id, toId: this.data.name, fileName: '',
         fileSize: 0, fileType: ''
       };
       formData.append('message', JSON.stringify({message: this.message, fromId: this.data.id, toId: this.data.name}));
       formData.append('file', this.selectedFiles.item(0));
       this.uploadService.pushFileToStorage(formData).subscribe(data => {
         this.selectedFiles = undefined;
         console.log(data);
       });
     }
   }
 */
    /*closechat(event: MouseEvent): void{
      this.stompClient.disconnect(() => {
        console.log('Websocket Disconnected');
      });
      this.bottomSheetRef.dismiss();
      event.preventDefault();
    }

    selectFile(event): void {
      if (event.target.files.length > 0) {
        this.selectedFiles = event.target.files;
      }
    }*/
  }
  handleResult(message): void{
    if (message.body) {
      const messageResult: Message = JSON.parse(message.body);
      this.messages.push(messageResult);
    }
  }
}
