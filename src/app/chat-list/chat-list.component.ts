import {Component, OnInit} from '@angular/core';
import {ChatService} from '../services/chat.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {ChatListPopOverComponent} from '../components/chat-list-pop-over/chat-list-pop-over.component';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss']
})
export class ChatListComponent implements OnInit {

  chatData: any = [];
  loginId: any;
  constructor(private service: ChatService, private router: Router, private bottomSheet: MatBottomSheet,
              private activeRoute: ActivatedRoute, private dataService: DataService) {
    this.service.getData().then(data => {
      this.chatData = data;
    });
  }
  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe(params => {
      this.loginId = params.loginId;
    });
    this.dataService.changeMessage(this.loginId);
  }
  openBottomSheet(data, image): void {
    this.bottomSheet.open(ChatListPopOverComponent, {
      data: {
        name: data.name,
        image,
        id: this.loginId
      },
    });
  }
  openChat(data, image) {
    window.parent.postMessage(
      {
          event_id: 'my_cors_message',
          data: {
              v1: 'http://localhost:4200/chat?name=' + data.name + '&image=' + image + '&id=' + this.loginId,
              v2: 'value2'
          }
      },
      '*'
  );
   // this.router.navigate(['chat'], { queryParams: { name: data.name, image} });
  }
}
