import { Component, OnInit } from '@angular/core';
import {Message} from '../model/Message';
import {environment} from '../../environments/environment';
import {DataService} from '../services/data.service';
import {ChatService} from '../services/chat.service';
import {ActivatedRoute, Router} from '@angular/router';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import { saveAs } from 'file-saver';
import {SocketService} from '../services/socket.service';
import {UploadfileService} from '../services/uploadfile.service';
import { DomSanitizer } from '@angular/platform-browser';
import {MatDialog} from '@angular/material/dialog';
import {ForwardpopupComponent} from '../components/forwardpopup/forwardpopup.component';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  data: any = {};
  userMessages: any = [];
  username = null;
  message = '';
  stompClient = null;
  isLoaded = false;
  isCustomSocketOpened = false;
  messages: Message[] = [];
  selectedFiles: FileList;
  private serverUrl = environment.url + 'ws';
  file = [];
  constructor(private route: ActivatedRoute,
              public sanitizer: DomSanitizer, private router: Router,
              public dialog: MatDialog,
              private service: ChatService, private dataService: DataService,
              private socketService: SocketService, private uploadService: UploadfileService, private activeRoute: ActivatedRoute) {

    this.activeRoute.queryParams.subscribe(params => {
      this.data = params;
    });
  }

  ngOnInit(): void {
    window.addEventListener('message', function(event) {
     // alert(`Received ${event.data} from ${event.origin}`);
    });

   // this.dataService.currentMessage.subscribe(username => this.username = username);
    const ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    this.stompClient.connect({}, () => {
      that.isLoaded = true;
      that.openSocket();
    });
    console.log(this.data.id);
  }
  openSocket(): void {
    if (this.isLoaded) {
      this.isCustomSocketOpened = true;
      this.stompClient.subscribe('/socket-publisher/' + this.data.id, (message) => {
        this.handleResult(message);
      });
    }
  }
  sendMessage(): void {
    const today = Date.now();
    if (this.data.id) {
      const message: Message = { message: this.message, fromId: this.data.id, toId: this.data.name, fileName: '',
        fileSize: 0 , fileType: '',
        sentTime: today, fileURL: '', finalImage: '', encodedString: ''};
      this.stompClient.send('/socket-publisher/send/message', {}, JSON.stringify(message));
    }
  }

  sendMessageUsingRest(): void {

  console.log(1);
  const formData: FormData = new FormData();
  const today = Date.now();
  if (this.message !== '') {
      console.log(2);
      if (this.selectedFiles === undefined) {
        console.log(3);
        if (this.data.id) {
          console.log(4);
          const message: Message = {
            message: this.message, fromId: this.data.id, toId: this.data.name, fileName: '',
            fileSize: 0, fileType: '', sentTime: today, fileURL: '', finalImage: '', encodedString: ''
          };
          this.socketService.post(message).subscribe(res => {
            this.selectedFiles = undefined;
            this.message = '';
            console.log(res);
          });
        }
      } else {

        formData.append('message', JSON.stringify({message: this.message, fromId: this.data.id, toId: this.data.name,
          sentTime: today}));
        formData.append('file', this.selectedFiles.item(0));
        this.uploadService.pushFileToStorage(formData).subscribe(data => {
          this.selectedFiles = undefined;
          this.message = '';
          console.log(data);
        });
      }
    }else if (this.message === '' && this.selectedFiles !== undefined){
      console.log(6);
      formData.append('message', JSON.stringify({message: this.message, fromId: this.data.id, toId: this.data.name,
        sentTime: today}));
      formData.append('file', this.selectedFiles.item(0));
      this.uploadService.pushFileToStorage(formData).subscribe(data => {
        this.selectedFiles = undefined;
        this.message = '';
        console.log(data);
      });
    }
  }

  closechat(event: MouseEvent): void{
    window.parent.postMessage(
      {
          event_id: 'Chatclose',
          data: {
              v1: 'value1',
              v2: 'value2'
          }
      },
      '*'
  );
    this.stompClient.disconnect(() => {
      console.log('Websocket Disconnected');
    });
    this.router.navigate(['chat-list'], { queryParams: { loginId: this.data.id} });
    event.preventDefault();
  }

  selectFile(event): void {
    if (event.target.files.length > 0) {
      this.selectedFiles = event.target.files;
    }
  }

  handleResult(message): void{
    if (message.body) {
      const messageResult: Message = JSON.parse(message.body);
      messageResult.finalImage = this.sanatizeUrl(messageResult.encodedString, messageResult.fileType);
      this.messages.push(messageResult);
    }
  }


  sanatizeUrl(encodedString: string, fileType: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl('data:' + fileType + ';base64,' + encodedString);
  }

  deleteMsg(index: number) {
    this.messages.splice(index, 1);
  }

  saveAttachment(fileName: string) {
   this.uploadService.getFiles(fileName).subscribe(data => {
    // const contentDispositionHeader: string = data.headers.get('Content-Disposition');
     const contentType: string = data.type;
   //  const parts: string[] = contentDispositionHeader.split(';');
   //  const filename = parts[1].split('=')[1];
     const blob = new Blob([data], { type: contentType });
    // const fileURL = URL.createObjectURL(blob);
     // window.open(fileURL);
     saveAs(blob, fileName);
     console.log(data);
   });
  }
  forwordpopup(message: string): void {
    const dialogRef = this.dialog.open(ForwardpopupComponent, {
      // width: '250px',
      panelClass: 'forward',
      data: {
        msg: message,
        fImage: this.data.image
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

}
