import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UploadfileService {
  private userServiceUrl = environment.url;

  constructor(private http: HttpClient) {
  }

  pushFileToStorage(formdata: FormData) {
    return this.http.post(this.userServiceUrl + 'api/ws/post', formdata).pipe(map(response => {
      return response;
    }), catchError(() => of('Error')));
  }
  getFiles(fileName: string): Observable<any> {
    return this.http.get(this.userServiceUrl + 'api/ws/files/' + fileName, { responseType: 'blob' as 'json'});
  }
}
