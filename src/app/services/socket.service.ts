import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Message} from '../model/Message';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  url: string = environment.url + 'api/ws';

  constructor(private http: HttpClient) { }

  post(data: Message) {
    return this.http.post(this.url, data)
      .pipe(map((msg: Message) => data), catchError( error => {
        return throwError( 'Something went wrong!' );
      }));
  }
}
