import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  getData() {
    return new Promise((resolve, reject) => {
      resolve([
        {
          picture: 'https://i.pravatar.cc/400',
          name: 'Lilian Pena',
          email: 'lilianpena@anarco.com',
          phone: '+91 (854) 467-2747',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-04-29',
          onlineStatus: 'Online',
          chatcount: 2
        },
        {
          picture: 'https://i.pravatar.cc/300',
          name: 'Kitty Stewart',
          email: 'kittystewart@anarco.com',
          phone: '+91 (960) 521-3200',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-04-28',
          onlineStatus: 'Online',
          chatcount: 2
        },
        {
          picture: 'https://i.pravatar.cc/600',
          name: 'Woodward Compton',
          email: 'woodwardcompton@anarco.com',
          phone: '+91 (924) 520-2358',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-02-16',
          onlineStatus: 'Online',
          chatcount: 0
        },
        {
          picture: 'https://i.pravatar.cc/300',
          name: 'Snow Marsh',
          email: 'snowmarsh@anarco.com',
          phone: '+91 (863) 491-2675',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-07-07',
          onlineStatus: 'Online',
          chatcount: 0
        },
        {
          picture: 'https://i.pravatar.cc/800',
          name: 'Smith Kirby',
          email: 'smithkirby@anarco.com',
          phone: '+91 (885) 401-3108',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-04-12',
          onlineStatus: 'Online',
          chatcount: 0
        },
        {
          picture: 'https://i.pravatar.cc/300',
          name: 'Goff Ortega',
          email: 'goffortega@anarco.com',
          phone: '+91 (812) 543-3310',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-05-14',
          onlineStatus: 'Online',
          chatcount: 0
        },
        {
          picture: 'https://i.pravatar.cc/600',
          name: 'Sharp Cooley',
          email: 'sharpcooley@anarco.com',
          phone: '+91 (949) 520-2914',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-01-02',
          onlineStatus: 'Online',
          chatcount: 0
        },
        {
          picture: 'https://i.pravatar.cc/300',
          name: 'Julie Harmon',
          email: 'julieharmon@anarco.com',
          phone: '+91 (868) 437-3677',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-06-22',
          onlineStatus: 'read',
          chatcount: 0
        },
        {
          picture: 'https://i.pravatar.cc/800',
          name: 'Simpson Irwin',
          email: 'simpsonirwin@anarco.com',
          phone: '+91 (904) 543-3029',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-08-10',
          onlineStatus: 'Online',
          chatcount: 0
        },
        {
          picture: 'https://i.pravatar.cc/300',
          name: 'Goff Ortega',
          email: 'goffortega@anarco.com',
          phone: '+91 (812) 543-3310',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-05-14',
          onlineStatus: 'Online',
          chatcount: 0
        },
        {
          picture: 'https://i.pravatar.cc/400',
          name: 'Sharp Cooley',
          email: 'sharpcooley@anarco.com',
          phone: '+91 (949) 520-2914',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-01-02',
          onlineStatus: 'Online',
          chatcount: 0
        },
        {
          picture: 'https://i.pravatar.cc/300',
          name: 'Julie Harmon',
          email: 'julieharmon@anarco.com',
          phone: '+91 (868) 437-3677',
          status: 'Magna fugiat dolor quis minim ad adipisicing incididunt eiusmod sunt aliquip sunt aute ad aute.' ,
          time: '2019-06-22',
          onlineStatus: 'Online',
          chatcount: 0
        }
      ]);
    });
  }
  getChatData(image, name) {
    return new Promise((resolve, reject) => {
      resolve([
        {
          name: 'Sellers Finley',
          text: 'Hello',
          time: '2019-05-28 07:18 am'
        },
        {
          name: 'anyName',
          text: 'Hi ' + name,
          time: '2019-05-31 12:54 pm'
        },
        {
          name: 'anyName',
          text: 'nyc profile picture',
          time: '2019-05-03 03:59 am'
        },
        {
          name: 'Sellers Finley',
          text: 'thanks dear',
          time: '2019-04-03 11:03 am'
        },
        {
          name: 'Sellers Finley',
          text: 'can you please send me this picture?',
          time: '2019-02-25 05:24 am'
        },
        {
          name: 'anyName',
          text: 'yeah why not',
          time: '2019-05-20 06:31 am'
        },
        {
          name: 'anyName',
          text: '',
          image,
          time: '2019-04-07 12:48 pm'
        },
        {
          name: 'Sellers Finley',
          text: 'Thanks',
          time: '2019-08-19 12:13 pm'
        },
        {
          name: 'Sellers Finley',
          text: 'Your Welcome..!!',
          time: '2019-05-28 04:01 pm'
        }
      ]);
    });
  }
}
