import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private userName = new BehaviorSubject('default user');
  currentMessage = this.userName.asObservable();

  constructor() { }

  changeMessage(userName: string) {
    this.userName.next(userName);
  }
}
