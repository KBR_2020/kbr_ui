export interface Message {
  message: string;
  fromId: string;
  toId: string;
  fileName: string;
  fileSize: number;
  fileType: string;
  sentTime: number;
  fileURL: string;
  encodedString: string;
  finalImage: any;
}
