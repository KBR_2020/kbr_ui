import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ChatListComponent} from './chat-list/chat-list.component';
import {ChatComponent} from './chat/chat.component';


const routes: Routes = [
  { path: 'chat-list', component: ChatListComponent },
  { path: 'chat', component: ChatComponent },
  { path: '', redirectTo: '/chat-list', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
